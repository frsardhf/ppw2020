from django import forms
from .models import Course, Activity, Person


class PersonForm(forms.ModelForm):
    class Meta:
        model = Person
        fields = (
            'name',
        )

        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control'}),
        }


class CourseForm(forms.ModelForm):
    class Meta:
        model = Course
        fields = (
            'name',
            'lecturer',
            'credit',
            'description',
            'year',
            'room',
        )

        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control'}),
            'lecturer': forms.TextInput(attrs={'class': 'form-control'}),
            'credit': forms.TextInput(attrs={'class': 'form-control'}),
            'description': forms.Textarea(attrs={'class': 'form-control'}),
            'year': forms.TextInput(attrs={'class': 'form-control'}),
            'room': forms.TextInput(attrs={'class': 'form-control'}),
        }