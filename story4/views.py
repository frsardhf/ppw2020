from django.http import Http404
from django.shortcuts import render, redirect, get_object_or_404
from django.forms import inlineformset_factory, modelformset_factory
from django.contrib.auth.decorators import login_required
from .forms import CourseForm, PersonForm
from .models import Course, Activity, Person
#TODO Implement


def index(request):
    return render(request, 'story4/index.html')


def gallery(request):
    return render(request, 'story4/gallery.html')

@login_required(login_url='story7:login')
def course_add(request):
    form = CourseForm()
    if request.method == "POST":
        form = CourseForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('profile:course')
    context = {
        'form': form
    }
    return render(request, 'story4/form.html', context)


@login_required(login_url='story7:login')
def course_update(request, pk):
    course = Course.objects.get(id=pk)
    form = CourseForm(instance=course)
    if request.method == "POST":
        form = CourseForm(request.POST, instance=course)
        if form.is_valid():
            form.save()
            return redirect('profile:course')
    context = {
        'course': course,
        'form': form
    }
    return render(request, 'story4/form.html', context)


@login_required(login_url='story7:login')
def course_delete(request, pk):
    course = Course.objects.get(id=pk)
    if request.method == "POST":
        course.delete()
        return redirect('profile:course')
    context = {
        'course': course
    }
    return render(request, 'story4/delete.html', context)


@login_required(login_url='story7:login')
def course_detail(request, pk):
    course = Course.objects.get(id=pk)
    context = {
        'course': course
    }
    return render(request, 'story4/detail.html', context)

def course(request):
    course = Course.objects.all()
    context = {
        'course': course
    }
    return render(request, 'story4/course.html', context)

def activity(request):
    activity_1 = Person.objects.filter(activity__name="DOTA")
    activity_2 = Person.objects.filter(activity__name="LoL")
    activity_3 = Person.objects.filter(activity__name="Genshin")
    context = {'activity_1': activity_1, 'activity_2': activity_2, 'activity_3': activity_3,}
    return render(request, 'story4/activity.html', context)


@login_required(login_url='story7:login')
def register(request, pk):
    PersonFormSet = inlineformset_factory(Activity, Person, fields=('name',))
    activity = Activity.objects.get(id=pk)
    formset = PersonFormSet(instance=activity)
    if request.method == "POST":
        formset = PersonFormSet(request.POST, instance=activity)
        if formset.is_valid():
            formset.save()
            return redirect('profile:activity')
    return render(request, 'story4/register.html', {'formset': formset})

