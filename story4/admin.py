from django.contrib import admin

# Register your models here.
from story4.models import Course, Activity, Person
admin.site.register(Course)
admin.site.register(Person)
admin.site.register(Activity)