from django.contrib import admin
from django.urls import path, include
from . import views

#url for app, add your URL Configuration
app_name = 'profile'

urlpatterns = [
    #TODO Implement this
    path('', views.index, name='index'),
    path('gallery/', views.gallery, name='gallery'),
    path('course/add', views.course_add, name='add_course'),
    path('course/update/<str:pk>', views.course_update, name='update_course'),
    path('course/delete/<str:pk>', views.course_delete, name='delete_course'),
    path('course/detail/<str:pk>', views.course_detail, name='detail_course'),
    path('course/', views.course, name='course'),
    path('activity/', views.activity, name='activity'),
    path('register/<str:pk>', views.register, name='register'),
]
