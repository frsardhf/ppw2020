from selenium import webdriver
from django.test import TestCase, LiveServerTestCase, TestCase, tag, Client
from django.urls import reverse, resolve
from django.http import HttpRequest
from selenium import webdriver
from .models import Course, Activity, Person
from .forms import CourseForm, PersonForm   
from .views import index, gallery, course, course_add, course_delete, course_detail, course_update
import unittest


class ModelTest(TestCase):

    def setUp(self):
        self.course = Course.objects.create(name='ppw', lecturer='bu ara',
        credit='3', description='lagi belajar unittest', year='2020',
        room='rumah')

    def test_instance_created(self):
        self.assertEqual(Course.objects.count(), 1)

    def test_str(self):
        self.assertEqual(str(self.course), 'ppw')


class ViewsTest(TestCase):
    def setUp(self):
        self.client = Client()
        self.c = Course.objects.create(name='ppw', lecturer='bu ara',
        credit='3', description='lagi belajar unittest', year='2020', room='rumah')
        self.index = reverse('profile:index')
        self.gallery = reverse('profile:gallery')
        self.course = reverse('profile:course')
        self.add_course = reverse('profile:add_course')
        self.detail_course = reverse('profile:detail_course', args=[self.c.id])
        self.update_course = reverse('profile:update_course', args=[self.c.id])
        self.delete_course = reverse('profile:delete_course', args=[self.c.id])
        self.activity = reverse('profile:activity')

    def test_GET_index(self):
        response = self.client.get(self.index)
        self.assertEqual(response.status_code, 200)

    def test_GET_gallery(self):
        response = self.client.get(self.gallery)
        self.assertEqual(response.status_code, 200)
    
    def test_GET_course(self):
        response = self.client.get(self.course)
        self.assertEqual(response.status_code, 200)

    def test_GET_add_course(self):
        response = self.client.post(self.add_course, follow=True)
        self.assertEqual(response.status_code, 200)

    def test_GET_detail_course(self):
        response = self.client.get(self.detail_course, follow=True)
        self.assertEqual(response.status_code, 200)

    def test_POST_detail_course(self):
        response = self.client.post(self.detail_course, {
            'name': 'ppw',
            'lecturer': 'bu ara',
            'credit': '3',
            'description': 'i love ppw',
            'year': '2020',
            'room': 'rumah',
        }, follow=True)
        self.assertEqual(response.status_code, 200)

    def test_GET_update_course(self):
        response = self.client.get(self.update_course, follow=True)
        self.assertEqual(response.status_code, 200)

    def test_POST_update_course(self):
        response = self.client.post(self.update_course, {
            'name': 'ppw',
            'lecturer': 'bu ara',
            'credit': '3',
            'description': 'i love ppw',
            'year': '2020',
            'room': 'rumah',
        }, follow=True)
        self.assertEqual(response.status_code, 200)

    def test_GET_delete_course(self):
        response = self.client.get(self.delete_course, follow=True)
        self.assertEqual(response.status_code, 200)

    def test_POST_add_course(self):
        response = self.client.post(self.delete_course, {
            'name': 'ppw',
            'lecturer': 'bu ara',
            'credit': '3',
            'description': 'i love ppw',
            'year': '2020',
            'room': 'rumah',
        }, follow=True)
        self.assertEqual(response.status_code, 200)

    def test_GET_activity(self):
        self.activity_1 = Activity.objects.create(name='DOTA')
        self.activity_2 = Activity.objects.create(name='LoL')
        self.activity_3 = Activity.objects.create(name='Genshin')
        self.assertEqual(Activity.objects.count(), 3)
        response = self.client.get(self.activity)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'story4/activity.html')
