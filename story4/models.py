from django.db import models

# Create your models here.


class Activity(models.Model):

    class Meta:
        verbose_name_plural = 'Activities'
    name = models.CharField(max_length=50)
    

class Person(models.Model):

    name = models.CharField(max_length=50)

    activity = models.ForeignKey(Activity, on_delete=models.CASCADE)


class Course(models.Model):
    name = models.CharField(max_length=50)
    lecturer = models.CharField(max_length=80)
    credit = models.CharField(max_length=10)
    description = models.TextField()
    year = models.CharField(max_length=20)
    room = models.CharField(max_length=10)

    def __str__(self):
        return self.name
