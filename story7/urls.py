from django.urls import path

from . import views

app_name = 'story7'

urlpatterns = [
    path('accordion/', views.accordion, name='accordion'),
    path('booksfinder/', views.booksfinder, name='booksfinder'),
    path('user/login', views.loginpage, name='login'),
    path('user/logout', views.logoutuser, name='logout'),
    path('user/register', views.registeruser, name='registeruser'),
]
