$(".searchbox").keyup(function (e) {
    if (e.keyCode == 13) {
        searchQuery($("#search").val());
    }
});

function searchQuery(titleSearch) {
    $.ajax({
        url: "https://www.googleapis.com/books/v1/volumes?q=" + titleSearch,
        dataType: "json",

        success: function (data) {
            console.log(data);

            var tbody = $(".result");
            tbody.empty();
            for (i = 0; i < data.items.length; i++) {
                tbody.append("<tr>");

                var title = data.items[i].volumeInfo.title;
                try {
                    var cover = data.items[i].volumeInfo.imageLinks.smallThumbnail;
                } catch (error) {
                    var cover = "https://i.ibb.co/McfrGBF/noImage.jpg";
                }
                try {
                    var author = data.items[i].volumeInfo.authors[0];
                } catch (error) {
                    var author = "No Author";
                }

                tbody.append("<th scope='row' class='table-dark'>" + (i + 1) + "</th>");
                tbody.append("<td class='table-dark'>" + "<img class='img-fluid' src=" + cover + ">" + "</td>")
                tbody.append("<td class='table-dark'>" + title + "</td>" + "<td class='table-dark'>" + author + "</td>");

                tbody.append("</tr>");
            }
        }
    })    
}
