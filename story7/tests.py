from django.urls import resolve, reverse
from django.test import TestCase, LiveServerTestCase
from django.contrib.auth.models import User
from django.test import Client
from story7.views import accordion, booksfinder
from story7.models import Book


class ModelTest(TestCase):

    def setUp(self):
        self.book = Book.objects.create(
            book_id="nl-ZtgEACAAJ",
            book_title="Hyouka",
            book_img="http://books.google.co.id/books?id=nl-ZtgEACAAJ&dq=hyouka&hl=&cd=1&source=gbs_api",
            book_link="https://www.googleapis.com/books/v1/volumes/nl-ZtgEACAAJ",
            count_like=1
        )

    def test_instance_created(self):
        self.assertEqual(Book.objects.count(), 1)

    def test_str(self):
        self.assertEqual(str(self.book), "Hyouka")


class ViewsTest(TestCase):

    def setUp(self):
        self.client = Client()
        self.book = Book.objects.create(
            book_id="nl-ZtgEACAAJ",
            book_title="Hyouka",
            book_img="http://books.google.co.id/books?id=nl-ZtgEACAAJ&dq=hyouka&hl=&cd=1&source=gbs_api",
            book_link="https://www.googleapis.com/books/v1/volumes/nl-ZtgEACAAJ",
            count_like=1
        )
        self.accordion = reverse('story7:accordion')
        self.books = reverse('story7:booksfinder')
        self.registeruser = reverse('story7:registeruser')
        self.loginpage = reverse('story7:login')
        self.logoutpage = reverse('story7:logout')

    def test_GET_accordion(self):
        response = self.client.get(self.accordion)
        self.assertEqual(response.status_code, 200)

    def test_GET_books(self):
        response = self.client.get(self.books)
        self.assertEqual(response.status_code, 200)

    def test_GET_login(self):
        response = self.client.get(self.loginpage)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "story7/login.html")

    def test_GET_register(self):
        response = self.client.get(self.registeruser)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "story7/registeruser.html")
    
    def test_POST_register_invalid(self):
        response = self.client.post(self.registeruser, {
            'username': 'frsardhafa',
            'password1': '12345678',
            'password2': '123458'
        }, follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "story7/registeruser.html")

    def test_POST_register_valid(self):
        response = self.client.post(self.registeruser, {
            'username': 'frsada',
            'password1': 'dha54321',
            'password2': 'dha54321'
        }, follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "story7/login.html")
        self.assertIn("Register Successful!", str(response.content))

    def test_POST_login_invalid(self):
        response = self.client.post(self.loginpage, {
            'username': 'frsardhafa',
            'password': 'passwordsalah',
        }, follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "story7/login.html")
        self.assertIn("Incorrect Username or Password", str(response.content))


    def test_POST_login_valid(self):
        user = User.objects.create(username='frsardhafa')
        user.set_password('dha54321')
        user.save()
        response = self.client.post(self.loginpage, {
            'username': 'frsardhafa',
            'password': 'dha54321',
        }, follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "story4/index.html")
        self.assertIn("frsardhafa", str(response.content))

    def test_GET_logout(self):
        # If a user login, this view should logout the user and redirect to loginpage
        user = User.objects.create(username='bruh')
        user.set_password('12345')
        user.save()
        self.client.login(username='bruh', password='12345')
        response = self.client.get(self.logoutpage, follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "story7/login.html")
