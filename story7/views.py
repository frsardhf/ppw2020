from django.shortcuts import render, redirect
from django.http import JsonResponse, HttpResponse
from django.core import serializers
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages

from .models import Book
from .forms import CreateUserForm

# Create your views here.


def accordion(request):
    return render(request, 'story7/accordion.html')

def booksfinder(request):
    return render(request, 'story7/booksfinder.html')

def loginpage(request):

    if request.method == "POST":
        username = request.POST.get('username')
        password = request.POST.get('password')
        
        user = authenticate(request, username=username, password=password)

        if user is not None:
            login(request, user)
            return redirect('profile:index')
        else:
            messages.info(request, "Incorrect Username or Password")

    return render(request, 'story7/login.html')

def logoutuser(request):
    logout(request)
    return redirect('story7:login')

def registeruser(request):
    if request.user.is_authenticated:
        return redirect ('profile:index')
        
    form = CreateUserForm()
    if request.method == "POST":
        form = CreateUserForm(request.POST)
        if form.is_valid():
            form.save()
            messages.success(request, "Register Successful!")

            return redirect('story7:login')

    context = {'form': form
    }
    return render(request, 'story7/registeruser.html', context)    
