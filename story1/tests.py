from django.urls import resolve, reverse
from django.test import Client, TestCase
from story1.views import index, calculate_age, curr_year

class ViewsTest(TestCase):

    def setUp(self):
        self.client = Client()
        self.story1 = reverse('story1:index')

    def test_GET_story1(self):
        response = self.client.get(self.story1)
        self.assertEqual(response.status_code, 200)

    
