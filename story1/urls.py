from django.urls import path, include
from . import views
#url for app
app_name = 'story1'

urlpatterns = [
    path('', views.index, name='index'),
]
